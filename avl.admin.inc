<?php

/**
 * @file
 * Admin setting code here
 */

module_load_include('inc', 'avl', 'avl.db');

/**
 * Settings form.
 */
function avl_field_setting_form() {

  drupal_add_js(drupal_get_path('module', 'avl') . '/js/avl.js');

  $form = array();

  $instances = field_info_instances();
  $field_types = field_info_field_types();
  $roles = user_roles(TRUE);
  $bundles_attached_with = array();
  $default_global_roles = array();
  $title = t('Appeared in');
  $type = 'ul';

  foreach ($roles as $rid => $value) {

    if (variable_get(AVL_ALL . '_' . $rid) != NULL) {
      $default_global_roles[] = $rid;
    }
  }

  $form['avl_form']['roles'] = array(
    '#title' => t('Roles'),
    '#type' => 'checkboxes',
    '#options' => $roles,
    '#title' => t('Roles you want to give permission to edit all Select list fields?'),
    '#attributes' => array('class' => array('roles-wrapper')),
    '#default_value' => $default_global_roles,
    '#after_build' => array('avl_checkbox_process'),
  );

  foreach ($instances as $obj_type => $type_bundles) {

    foreach ($type_bundles as $bundle_instances) {
      foreach ($bundle_instances as $field_name => $instance) {

        $field = field_info_field($field_name);
        $type = $field['type'];

        if (substr($type, 0, 4) === "list") {
          $default_roles = array();
          foreach ($roles as $rid => $rname) {
            if (_avl_is_perm_exist($field_name, $rid)) {
              $default_roles[] = $rid;
            }
          }
          $bundles_attached_with = $field['bundles'][$obj_type];
          $form['avl_form'][$field_name] = array(
            '#title' => check_plain($instance['label']),
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
          );
          $items = array();
          foreach ($bundles_attached_with as $value) {
            $items[] = l($value, avl_bundle_admin_path($obj_type, $value));
          }
          $form['avl_form'][$field_name][$field_name . '_adminpath_'] = array(
            '#markup' => theme('item_list', array(
              'items' => $items,
              'title' => $title,
              'type' => $type,
                )
            ),
          );
          $form['avl_form'][$field_name][$field_name] = array(
            '#title' => check_plain($field_types[$field['type']]['label']),
            '#type' => 'checkboxes',
            '#options' => $roles,
            '#default_value' => $default_roles,
            '#title' => t('Roles you want field to edit?'),
            '#attributes' => array('class' => array('avl-field-wrapper')),
            '#after_build' => array('avl_checkbox_process'),
          );
        }
      }
    }
  }

  if (count($form) == 0) {
    $form['markup'] = array(
      '#type' => 'markup',
      '#markup' => '<b>No Select list field found to edit</b>',
    );
    return $form;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}
/**
 * Implements checkbox process function. 
 */
function avl_checkbox_process($element) {

  foreach (element_children($element) as $key) {
    if ($element[$key]['#default_value']) {
      $element[$key]['#attributes'] = array('class' => array('default-checked'));
    }
  }
  return $element;
}

/**
 * Submit callback.
 */
function avl_field_setting_form_submit($form, &$form_state) {

  foreach ($form_state['values']['roles'] as $rid => $value) {
    if ($value != 0) {
      variable_set(AVL_ALL . '_' . $rid, 1);
    }
    else {
      variable_del(AVL_ALL . '_' . $rid);
    }
  }

  unset($form_state['values']['roles']);

  foreach ($form_state['values'] as $field_name => $roles) {
    if (substr($field_name, 0, 5) == "field") {
      foreach ($roles as $rid => $value) {
        if ($value != 0) {
          if (!_avl_is_perm_exist($field_name, $rid)) {
            _avl_insert_perm($field_name, $rid);
          }
        }
        else {
          if (_avl_is_perm_exist($field_name, $rid) && is_null(variable_get(AVL_ALL . '_' . $rid))) {
            if (_avl_delete_perm($field_name, $rid)) {
              drupal_set_message(t('deleted'), 'status');
            }
          }
        }
      }
    }
  }

  drupal_set_message(t('Permssion edited'), 'status');
}

/**
 * Determines the administration path for a bundle.
 */
function avl_bundle_admin_path($entity_type, $bundle_name) {
  $bundles = field_info_bundles($entity_type);
  $bundle_info = $bundles[$bundle_name];

  if (isset($bundle_info['admin'])) {
    return isset($bundle_info['admin']['real path']) ? $bundle_info['admin']['real path'] : $bundle_info['admin']['path'];
  }
}
