<?php

/**
 * @file
 * Contains db realted functions
 */

/**
 * Inserts field name and roles.
 */
function _avl_insert_perm($field_name, $rid) {

  $record = new stdClass();
  $record->rid = $rid;
  $record->field_name = $field_name;
  $saved_new = drupal_write_record(AVL_TABLE, $record);
  if ($saved_new == SAVED_NEW) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Checks is permission exists.
 */
function _avl_is_perm_exist($field_name, $rid) {
  $row_count = db_select(AVL_TABLE, 'avl')
          ->fields('avl')
          ->condition('rid', $rid, '=')
          ->condition('field_name', $field_name, '=')
          ->execute()->rowCount();
  return $row_count > 0 ? TRUE : FALSE;
}

/**
 * Deletes permission.
 */
function _avl_delete_perm($field_name, $rid) {
  $query = db_delete(AVL_TABLE);
  $query->condition('rid', $rid);
  $query->condition('field_name', $field_name);
  return $query->execute();
}

/**
 * Provides feilds assigned to logged in user roles.
 */
function _avl_get_fields($logged_in_user_roles) {
  $fields = array();
  $query = db_select(AVL_TABLE, 'avl');
  $query->fields('avl', array('field_name'));
  $or = db_or();

  foreach ($logged_in_user_roles as $rid => $value) {
    $or->condition('avl.rid', $rid, '=');
  }
  $query->condition($or);
  $query->distinct();

  $fields = $query->execute()->fetchCol();
  return $fields;
}

/**
 * Checks if user has access to edit the field.
 */
function _avl_user_access($logged_in_user_roles, $field_name) {
  $row_count = 0;
  $query = db_select(AVL_TABLE, 'avl');
  $query->fields('avl', array('field_name'));
  $or = db_or();

  foreach ($logged_in_user_roles as $rid => $value) {
    $or->condition('avl.rid', $rid, '=');
  }
  $query->condition($or);
  $query->condition('avl.field_name', $field_name, '=');

  $row_count = $query->execute()->rowCount();
  return $row_count == 0 ? FALSE : TRUE;
}

/**
 * Delete field. 
 */
function _avl_delete_field($field_name) {
  $query = db_delete(AVL_TABLE);
  $query->condition('field_name', $field_name);
  return $query->execute();
}
