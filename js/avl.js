/**
 * @file
 * The theme system, which controls the output of Drupal
 * file contains js code
 */


jQuery(document).ready(function() {
  jQuery('.roles-wrapper input[type="checkbox"]').change(function() {
    if (this.checked) {

      var val = this.value;
      if (jQuery(this).hasClass('default-checked') == false) {
        jQuery('.avl-field-wrapper input.default-checked[value="' + val + '"]').attr('disabled', 'disabled');
      }

      jQuery('.avl-field-wrapper input[value="' + val + '"]').attr('checked', 'checked')
    } else {
      var val = this.value;
      if (jQuery(this).hasClass('default-checked')) {
        jQuery('.avl-field-wrapper input[value="' + val + '"]').removeAttr('checked');
        return;
      }
      jQuery('.avl-field-wrapper input.default-checked[value="' + val + '"]').removeAttr("disabled");
      jQuery('.avl-field-wrapper input[value="' + val + '"]').not('.default-checked').removeAttr('checked');
    }
  });
  jQuery('.avl-field-wrapper input').change(function() {
    if (this.checked) {

    } else {
      var val = this.value;
      jQuery('.roles-wrapper input.default-checked[value="' + val + '"]').removeAttr("checked");
    }
  });
});
