<?php

/**
 * @file
 * Page callback here
 */

module_load_include('inc', 'avl', 'avl.db');

/**
 * Page callback.
 */
function avl_field_choose() {
  global $user;
  $fields = array();
  $rows = array();
  $headers = array(t('Field name'), t('Edit link'));
  $logged_in_user_roles = $user->roles;

  $fields = _avl_get_fields($logged_in_user_roles);

  if (is_array($fields) && count($fields)) {
    foreach ($fields as $field_name) {
      $rows[$field_name]['data'][0] = $field_name;
      $rows[$field_name]['data'][1] = l(t('Edit'), 'avl/' . $field_name . '/edit');
    }
    return theme('table', array('header' => $headers, 'rows' => $rows));
  }
  else {
    return t('No field added yet for you to edit');
  }
}

/**
 * Page callback.
 */
function avl_field_edit() {
  $fn = arg(1);
  $info = field_info_field($fn);

  $values = &$info['settings']['allowed_values'];

  $items = array();

  foreach ($values as $v) {
    $items[] = array('data' => $v);
  }

  $form['sparxsys_permission_field_form']['list'] = array(
    '#prefix' => '<div>',
    '#markup' => theme('item_list', array('items' => $items, NULL, 'ol')),
    '#suffix' => '</div>',
  );

  $form['sparxsys_permission_field_form']['new_options'] = array(
    '#title' => t('New data'),
    '#type' => 'textarea',
    '#description' => t('Add new data in field in comma separated format like shimla,delhi,mumbai'),
    '#cols' => 40,
    '#rows' => 3,
    '#resizable' => FALSE,
    '#required' => TRUE,
    '#cols' => 3,
    '#rows' => 4,
  );
  $form['sparxsys_permission_field_form']['fn'] = array(
    '#type' => 'hidden',
    '#default_value' => $fn,
    '#attributes' => array('readonly' => 'readonly'),
    '#required' => TRUE,
  );

  $form['sparxsys_permission_field_form']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add data'),
  );

  return $form;
}

/**
 * Implements hook_validate().
 */
function avl_field_edit_validate(&$form, &$form_state) {

  $form_values = $form_state['values'];
  $new_options = trim($form_values['new_options']);
  $info = field_info_field($form_values['fn']);
  $prev_values = &$info['settings']['allowed_values'];
  $new_options_exploded = explode(',', $new_options);

  foreach ($new_options_exploded as $s) {
    $trimmed = trim($s);
    if (in_array($trimmed, $prev_values)) {
      $message = t('@t is already in fields options', array('@t' => $s));
      form_set_error('new_options', $message);
    }
  }
}

/**
 * Submit handler for form.
 */
function avl_field_edit_submit(&$form, &$form_state) {

  $form_values = $form_state['values'];
  $new_options = trim($form_values['new_options']);
  $info = field_info_field($form_values['fn']);
  $prev_values = &$info['settings']['allowed_values'];
  $merged_array = array();

  $new_options_exploded = explode(',', $new_options);
  $v = array();
  foreach ($new_options_exploded as $s) {
    $v[] = trim($s);
  }
  $new_options_exploded = $v;
  $arr_to_append = array_combine($new_options_exploded, $new_options_exploded);

  $merged_array = array_merge($prev_values, $arr_to_append);
  $prev_values = $merged_array;

  try {
    field_update_field($info);
    drupal_set_message(t('Data added in field'));
  }
  catch (Exception $exc) {
    echo $exc->getTraceAsString();
  }
}
