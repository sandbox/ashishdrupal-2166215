This module is useful when you want to give users of certain role the rights to 
add the values in the field of type "select list", without giving them the 
permission to "Administer content type" which would be dangerous to give to
non-developers especially clients.

Many drupal developers and service providers would understand the need of 
this functionality.

Documentation

    Download the module as usual
    Enable the module
    Give permissions to specific roles who can use this module
    Go to the configuration page of this module where you will see the list of 
    all the fields of type "select list" 
    For each field listed on that page, checboxes can be checked to give 
   permission to specific roles who can add the values of that particular fields
    Once the permissions in above step is given to the roles, the users in that 
    role can navigate to PATH and they can see the fields, whose values they can
   add, for that they can click on "edit" link and add more values in the next 
  screen

Wish List Features
Currently this modules supports only adding new values but editing existing 
values is not supported. We are looking forward to add that functionality too.

This Module is sponsored by Sparxsys Solutions Pvt. Ltd.
